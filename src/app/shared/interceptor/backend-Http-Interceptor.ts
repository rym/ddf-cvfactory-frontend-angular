// tslint:disable-next-line:import-blacklist

import { Router } from '@angular/router';
import { Injectable, Injector } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpHeaders
} from '@angular/common/http';
import { CognitoUtil } from '../services/cognito/cognito.service';
import { Observable, from } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BackendHttpInterceptor implements HttpInterceptor {
  constructor(private router: Router, private cognitoUtil: CognitoUtil) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return from(this.cognitoUtil.getAccessToken()).pipe(
      mergeMap(token => {
        const authReq = req.clone({
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: token
          })
        });

        // send the newly created request
        return next.handle(authReq).pipe(
          catchError((error, caught) => {
            // intercept the respons error and displace it to the console
            console.log('Error Occurred');
            console.log(error);
            if (error.status === 401 || error.status === 403) {
              this.router.navigate(['/login']);
            }
            // return the error to the method that called it
            // tslint:disable-next-line: deprecation
            return Observable.throw(error);
          })
        );
      })
    );
  }
}
