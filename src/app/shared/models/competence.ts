
export class Competences{
  mandatoryCompetences: MandatoryCompetence[]=[]; 
  otherCompetences: OtherCompetence[]=[];
}

export class MandatoryCompetence {
  label: string;
  note: number;

}
export class OtherCompetence{
 label: string;
}
