import { Cv } from './cv';

export class User {
    email: string;
    nom: string;
    prenom: string;
    pole: any;
    password: string;
    confirmPassword: string;
    poleName: string;
    cvs: Cv[];
    role: Role;
  }


export class UserDto {
    user: User;
}

export class UsersDto {
  data: User[];
}

export enum Role {
  ADMIN = 'ADMIN',
  USER = 'USER'
}
