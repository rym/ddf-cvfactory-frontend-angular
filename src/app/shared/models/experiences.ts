export class Experience{
    anneesExperiencedeb: number;
    anneesExperiencefin: number;
    moisExperiencedeb: number;
    moisExperiencefin: number;
    anneeCourante: boolean;
    entreprise: string;
    poste: string;
    projet: string;
    technique: string;
    description: string;
}
