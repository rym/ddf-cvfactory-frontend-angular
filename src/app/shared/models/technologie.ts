export class Technologie{
    langageP: string;
    langageS: string;
    baseDonnee: string;
    methodologie: string;
    framework: string;
    git: string;
    build: string;
  }