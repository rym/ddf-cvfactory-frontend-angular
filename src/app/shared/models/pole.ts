export class Pole {
  id: string;
  name: string;
}

export class PoleDto {
  data: Pole[];
}
