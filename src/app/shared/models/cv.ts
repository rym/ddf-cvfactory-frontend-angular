import { Apropos } from './apropos';
import { Formation } from './formations';
import { Certification } from './certifications';
import { Technologie } from './technologie';
import { Competences } from './competence';
import { Experience } from './experiences';
import { Langue } from './langues';
import { Loisir } from './loisirs';

export class Cv {
  id: number;
  updateDate: Date;
  apropos: Apropos;
  formations: Formation[];
  certifications: Certification[];
  technologie: Technologie;
  competences: Competences;
  experiences: Experience[];
  langues: Langue[];
  loisirs: Loisir[];

}







