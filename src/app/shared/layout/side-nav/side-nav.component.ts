import { UserLoginService } from './../../services/cognito/user-login.service';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { CvService } from '../../services/cv.service';
import { UserService } from './../../services/users.service';
import { Cv } from 'src/app/shared/models/cv';
import { Role } from '../../models/user';
import { User } from 'src/app/shared/models/user';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent {
  constructor(private cvService: CvService,
    private router: Router,
    private userLoginService: UserLoginService,
    public userService: UserService) { }
  role = Role;
  user: User;
  monCv() {
    this.cvService.user = this.userService.currentUser;
    this.cvService.getCv(1, this.cvService.user.email).subscribe(cv => {
      if (cv) {
        this.cvService.cv$.next(cv);
      }
    });
  }
  loadProgressBar() {
    this.cvService.stateCv(this.cvService.cv$.getValue()).subscribe(cvValid => {
      if (cvValid) {
        this.cvService.cvValid$.next(cvValid);
      }
    });
  }

  logout() {
    localStorage.clear();
    this.userService.currentUser = null;
    this.userLoginService.sucessToLogin = false;
    this.router.navigateByUrl('/login');
  }

}
