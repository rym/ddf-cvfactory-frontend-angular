import { Injectable } from '@angular/core';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { User, UserDto, UsersDto } from '../models/user';
import { environment } from 'src/environments/environment';

const API_URL = environment.apiUrlUser;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })};

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public currentUser: User;

  constructor(private http: HttpClient) {
   }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
  getUsers(): Observable<UsersDto> {
    return this.http.get<UsersDto>(API_URL)
      .pipe(catchError(this.handleError('Users', null))
      );
  }

  getUser(email): Observable<UserDto> {
    return this.http.get<UserDto>(API_URL + `/${email}`)
      .pipe(
        catchError(this.handleError('Users', null))
      );
  }
  addUser(user): Observable<User> {
    return this.http
      .post<User>(API_URL + `/${user.email}`,  user  , httpOptions)
      .pipe(
        catchError(this.handleError('Users', null))
      );
  }
  deleteUser(email): Observable<UserDto> {
    return this.http.delete<UserDto>(API_URL + `/${email}`, httpOptions).pipe(
      tap(_ => console.log(`deleted user=${API_URL}/${email}`)),
      catchError(this.handleError('Users', null))
    );
  }
  cleanObject(obj: any) {
    for (const key in obj) {
      if (obj[key] === '') {
        obj[key] = null;
      }
    }
  }
  cleanList(listofObject: any) {
    for (const obj of listofObject) {
      this.cleanObject(obj);
    }
  }
}
