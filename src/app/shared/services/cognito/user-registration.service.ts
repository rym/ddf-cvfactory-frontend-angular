import { Inject, Injectable } from '@angular/core';
import { CognitoCallback, CognitoUtil } from './cognito.service';
import { AuthenticationDetails, CognitoUser, CognitoUserAttribute } from 'amazon-cognito-identity-js';
import * as AWS from 'aws-sdk/global';
import { UserService } from '../users.service';

@Injectable({
  providedIn: 'root'
})
export class UserRegistrationService {

    constructor( @Inject(CognitoUtil) public cognitoUtil: CognitoUtil,
                 private userService: UserService) {

    }

    register(user: any, callback: CognitoCallback): void {
        console.log('UserRegistrationService: user is ' + user);

        const attributeList = [];
        this.cognitoUtil.getUserPool().signUp(user.email, user.password, attributeList, null, (err, result) => {
            if (err) {
                callback.cognitoCallback(err.message, null);
            } else {
                console.log('UserRegistrationService: registered user is ' + result);
                callback.cognitoCallback(null, result);
            }
        });

    }

    confirmRegistration(username: string, confirmationCode: string, callback: CognitoCallback): void {

        const userData = {
            Username: username,
            Pool: this.cognitoUtil.getUserPool()
        };

        const cognitoUser = new CognitoUser(userData);

        cognitoUser.confirmRegistration(confirmationCode, true, (err, result) => {
            if (err) {
                callback.cognitoCallback(err.message, null);
            } else {
                callback.cognitoCallback(null, result);
            }
        });
    }

    resendCode(username: string, callback: CognitoCallback): void {
        const userData = {
            Username: username,
            Pool: this.cognitoUtil.getUserPool()
        };

        const cognitoUser = new CognitoUser(userData);

        cognitoUser.resendConfirmationCode((err, result) => {
            if (err) {
                callback.cognitoCallback(err.message, null);
            } else {
                callback.cognitoCallback(null, result);
            }
        });
    }

    newPassword(newPasswordUser: any): Promise<any> {
        return new Promise((resolve, reject) => {
            console.log(newPasswordUser);
            // Get these details and call
            // cognitoUser.completeNewPasswordChallenge(newPassword, userAttributes, this);
            const authenticationData = {
                Username: newPasswordUser.username,
                Password: newPasswordUser.existingPassword,
            };
            const authenticationDetails = new AuthenticationDetails(authenticationData);

            const userData = {
                Username: newPasswordUser.username,
                Pool: this.cognitoUtil.getUserPool()
            };

            console.log('UserLoginService: Params set...Authenticating the user');
            const cognitoUser = new CognitoUser(userData);
            console.log('UserLoginService: config is ' + AWS.config);
            cognitoUser.authenticateUser(authenticationDetails, {
                newPasswordRequired(userAttributes, requiredAttributes) {
                    // User was signed up by an admin and must provide new
                    // password and required attributes, if any, to complete
                    // authentication.

                    // the api doesn't accept this field back
                    delete userAttributes.email_verified;
                    cognitoUser.completeNewPasswordChallenge(newPasswordUser.password, requiredAttributes, {
                        onSuccess(result) {
                            resolve(result);
                        },
                        onFailure(err) {
                            resolve(err);
                        }
                    });
                },
                onSuccess(result) {
                    resolve(result);
                },
                onFailure(err) {
                    resolve(err);
                }
            });
        });
    }
}
