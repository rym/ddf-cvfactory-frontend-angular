import { Injectable } from '@angular/core';
import { CognitoCallback, CognitoUtil, LoggedInCallback } from './cognito.service';
import { AuthenticationDetails, CognitoUser, CognitoUserSession } from 'amazon-cognito-identity-js';
import * as AWS from 'aws-sdk/global';
import * as STS from 'aws-sdk/clients/sts';
import { Router } from '@angular/router';
import { UserRegistrationService } from './user-registration.service';
import { UserParametersService } from './user-parameters.service';

@Injectable({
  providedIn: 'root'
})
export class UserLoginService {

  sucessToLogin = false;
    constructor(public cognitoUtil: CognitoUtil, private router: Router,
                private userRegistrationService: UserRegistrationService,
                private userParamsService: UserParametersService) {
    }

    private onLoginSuccess = (session: CognitoUserSession) => {
        AWS.config.credentials = this.cognitoUtil.buildCognitoCreds(session.getIdToken().getJwtToken());
        const clientParams: any = {};
        const sts = new STS(clientParams);
        sts.getCallerIdentity((err, data) => {
            this.userParamsService.getUserData();
            this.router.navigate(['/list']);
        });
    }

    private onLoginError = (err) => {
        if (err.message) {
            return err.message;
        }
        return err;
    }

    authenticate(username: string, password: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const authenticationData = {
                Username: username,
                Password: password,
            };
            const authenticationDetails = new AuthenticationDetails(authenticationData);

            const userData = {
                Username: username,
                Pool: this.cognitoUtil.getUserPool()
            };
            const cognitoUser = new CognitoUser(userData);
            return cognitoUser.authenticateUser(authenticationDetails, {
                onSuccess: session => {
                    resolve(this.onLoginSuccess(session));
                    // resolve(this.verifyUserAttribute(cognitoUser, session, username));
                },
                onFailure: err => {
                    reject(this.onLoginError(err));
                },
                mfaRequired(codeDeliveryDetails) {
                    const verificationCode = prompt('Please input verification code', '');
                    cognitoUser.sendMFACode(verificationCode, this);
                },
                newPasswordRequired: newPasswordDetails => {
                    // tslint:disable-next-line:max-line-length
                    const newPassword = prompt('Your user was signed up \nby your organisation administrator \n\and you must provide new password.\n' +
                        // tslint:disable-next-line:max-line-length
                        'New password must contain at least 6 characters and must contain \nat least one capital letter, one letter in lower case, one special character and one numeric character.', '');
                    const newPasswordUser = {
                        username,
                        existingPassword: password,
                        password: newPassword
                    };
                    this.userRegistrationService.newPassword(newPasswordUser).then(res => {
                        resolve(this.onLoginSuccess(res));
                        // resolve(this.verifyUserAttribute(cognitoUser, res, username));
                    }).catch(err => {
                          reject(this.onLoginError('New Password is not valid. Please retry.'));
                      });
                }
            });
        });
    }

    verifyUserAttribute(cognitoUser, session, username): Promise<any> {
        const self = this;
        return new Promise((resolve, reject) => {
            cognitoUser.getAttributeVerificationCode('email', {
                onSuccess: verifyData => {
                    resolve(this.onLoginSuccess(session));
                },
                onFailure: dataError => {
                    reject(this.onLoginError(dataError));
                },
                inputVerificationCode() {
                    const verificationCode = prompt('Your email is not verified yet. Please input verification code that you have received : ', '');
                    cognitoUser.verifyAttribute('email', verificationCode, this);
                }
            });
        });
    }

    forgotPassword(username: string, callback) {
        const userData = {
            Username: username,
            Pool: this.cognitoUtil.getUserPool()
        };

        const cognitoUser = new CognitoUser(userData);

        cognitoUser.forgotPassword({
            onSuccess(result) {
                alert('Your password has been changed successfully!');
            },
            onFailure(err) {
                alert('An error has been occured while changing your password! Please try again');
            },
            inputVerificationCode() {
                callback((verificationCode, newPassword) => {
                    cognitoUser.confirmPassword(newPassword, verificationCode, this);
                });
            }
        });
    }

    logout() {
        this.cognitoUtil.getCurrentUser().signOut();

    }

    isAuthenticated(): Promise<boolean> {
        return new Promise((resolve, reject) => {

            const cognitoUser = this.cognitoUtil.getCurrentUser();

            if (cognitoUser != null) {
                cognitoUser.getSession((err, session) => {
                    if (err) {
                        this.router.navigate(['/login']);
                        resolve(false);
                    } else {
                        resolve(true);
                    }
                });
            } else {
                this.router.navigate(['/login']);
                resolve(false);
            }
        });
    }

    confirmNewPassword(email: string, verificationCode: string, password: string, callback: CognitoCallback) {
        const userData = {
            Username: email,
            Pool: this.cognitoUtil.getUserPool()
        };

        const cognitoUser = new CognitoUser(userData);

        cognitoUser.confirmPassword(verificationCode, password, {
            onSuccess() {
                callback.cognitoCallback(null, null);
            },
            onFailure(err) {
                callback.cognitoCallback(err.message, null);
            }
        });
    }
}
