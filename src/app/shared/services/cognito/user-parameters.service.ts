import { Injectable } from '@angular/core';
import { Callback, CognitoUtil } from './cognito.service';
import { CognitoUserAttribute } from 'amazon-cognito-identity-js';

@Injectable({
  providedIn: 'root'
})
export class UserParametersService {

    constructor(public cognitoUtil: CognitoUtil) {
    }

    getParameters(): Promise<CognitoUserAttribute[]> {

        return new Promise((resolve, reject) => {
            const cognitoUser = this.cognitoUtil.getCurrentUser();

            if (cognitoUser != null) {
                cognitoUser.getSession((err, session) => {
                    if (err) {
                        console.log('UserParametersService: Couldn\'t retrieve the user');
                        reject(null);
                    } else {
                        cognitoUser.getUserAttributes( (error, result) => {
                            if (error) {
                                console.log('UserParametersService: in getParameters: ' + err);
                                reject(null);
                            } else {
                                console.log('user attributes :: ', result);
                                resolve(result);
                            }
                        });
                    }

                });
            } else {
                reject(null);
            }
        });

    }


    getUserData(): Promise<any> {

        return new Promise((resolve, reject) => {
            const cognitoUser = this.cognitoUtil.getCurrentUser();

            if (cognitoUser != null) {
                cognitoUser.getSession((err, session) => {
                    if (err) {
                        console.log('UserParametersService: Couldn\'t retrieve the user');
                        reject(null);
                    } else {
                        cognitoUser.getUserData((error, result) => {
                            if (error) {
                                console.log('UserParametersService: in getParameters: ' + err);
                                reject(null);
                            } else {
                                resolve(result);
                            }
                        });
                    }

                });
            } else {
                reject(null);
            }
        });

    }
}
