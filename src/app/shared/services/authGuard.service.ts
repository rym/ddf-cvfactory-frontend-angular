import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { UserLoginService } from './cognito/user-login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  userService: any;
  constructor(private userLoginService: UserLoginService , private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.userLoginService.sucessToLogin) {
      this.router.createUrlTree(
        ['/login', { message: 'you do not have the permission to enter' }]
        // { skipLocationChange: true }
      );
    } else {
      return true;
    }
  }
}
