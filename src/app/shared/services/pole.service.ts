import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { Pole, PoleDto } from '../models/pole';

const API_URL = environment.apiUrlPole;
const httpOptions = {
headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PoleService {

  constructor(private http: HttpClient) {}

  private handleError(err) {
    console.log('this.handleError');
    return throwError(`An error occurred: ${err.error}`);
  }

  getPoles(): Observable<PoleDto> {
    return this.http.get<PoleDto>(API_URL).pipe(catchError(this.handleError));
  }

  getPole(id: string): Observable<Pole> {
    const url = `${API_URL}/${id}`;
    return this.http.get<Pole>(url)
    .pipe(
      catchError(this.handleError)
    );
  }

  addPole(pole): Observable<Pole> {
    return this.http
      .post<Pole>(API_URL,  pole  , httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  updatePole(id: string, pole): Observable<Pole> {
    const url = `${API_URL}/${id}`;
    return this.http
      .put<Pole>(url, {id, name: pole.name}  , httpOptions)
      .pipe(
        tap(_ => console.log(`fetched Pole id=${url}`, pole)),
        catchError(this.handleError));
  }

  deletePole(id: string): Observable<Pole> {
    const url = `${API_URL}/${id}`;
    console.log("id", id);
    return this.http.delete<Pole>(url, httpOptions)
    .pipe(
      tap(_ => console.log(`deleted pole id=${id}`)),
      catchError(this.handleError)
    );
  }
}
