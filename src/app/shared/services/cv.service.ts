import { Injectable } from '@angular/core';
import { Cv } from 'src/app/shared/models/cv';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { User } from '../models/user';
import { CvValid } from '../models/cvValid';
import { map } from 'rxjs/operators';

const API_URL = environment.apiUrlCv;
const API_URL_ProgressBAr = environment.apiUrlBarprogress;
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CvService {
  cv: Cv;
  value = 0;
  user: User;
  public cv$ = new BehaviorSubject<Cv>(new Cv);
  public cvValid$ = new BehaviorSubject<CvValid[]>([]);

  constructor(private http: HttpClient) { }

  private handleError(err) {
    console.log('this.handleError');
    return throwError(`An error occurred: ${err.error}`);
  }

  getCv(id: number, email: string): Observable<Cv> {
    return this.http.get<any>(`${API_URL}/${email}/cv/${id}`).pipe(
      map((cv) => cv.cvs[0]),
      catchError(this.handleError)
    );
  }

  updateCurrentValue(cv) {
    this.cv$.next(cv);
  }


  cleanObject(obj: any) {
    for (const key in obj) {
      if (obj[key] === '') {
        obj[key] = null;
      }
    }
  }

  cleanList(listofObject: any) {
    for (const obj of listofObject) {
      this.cleanObject(obj);
    }
  }

  updateCv(cv, email: string): Observable<User> {
    const url = `${API_URL}/${email}/cv/${cv.id}`;
    return this.http.put<User>(url, { cv }, httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  stateCv(cv): Observable<CvValid[]> {
    const url = `${API_URL_ProgressBAr}`;
    return this.http.put<CvValid[]>(url, {cv}, httpOptions).pipe(
      tap( // Log the result or error
        data => {
          this.cvValid$.next(data);
        },
        error => console.log(cv, error)
      )
    );
}
}
