
import { NgModule } from '@angular/core';


import {
  MatCheckboxModule,
  MatSelectModule,
  MatFormFieldModule,
  MatSortModule,
  MatTableModule,
  MatMenuModule,
  MatIconModule,
  MatSidenavModule,
  MatToolbarModule,
  MatListModule,
  MatStepperModule,
  MatButtonModule,
  MatInputModule,
  MatProgressBarModule,
  MatDialogModule
} from '@angular/material';
import { ChartsModule } from 'ng2-charts';


@NgModule({
  declarations: [

    ],
  imports: [
    ChartsModule,
    MatCheckboxModule,
    MatSelectModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatButtonModule,
    MatStepperModule,
    MatListModule,
    MatToolbarModule,
    MatSidenavModule,
    MatMenuModule,
    MatTableModule,
    MatSortModule,
    MatIconModule,
    MatInputModule,
    MatDialogModule
  ],
  providers: [
  ],
  bootstrap: [],
  exports: [
    ChartsModule,
    MatSortModule,
    MatCheckboxModule,
    MatSelectModule,
    MatProgressBarModule,
    MatFormFieldModule,
    MatButtonModule,
    MatStepperModule,
    MatListModule,
    MatToolbarModule,
    MatSidenavModule,
    MatMenuModule,
    MatTableModule,
    MatSortModule,
    MatIconModule,
    MatInputModule,
    MatDialogModule
  ]
})
export class SharedModule { }
