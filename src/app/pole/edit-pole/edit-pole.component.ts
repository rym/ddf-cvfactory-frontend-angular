import { Component, OnInit, EventEmitter, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, NgForm } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { PoleService } from 'src/app/shared/services/pole.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Pole } from './../../shared/models/pole';

@Component({
  selector: 'app-edit-pole',
  templateUrl: './edit-pole.component.html',
  styleUrls: ['./edit-pole.component.scss']
})
export class EditPoleComponent implements OnInit {
  editPoleForm: FormGroup;
  name: string;
  id: string;
  pole: Pole;
  event: EventEmitter<any> = new EventEmitter();

  constructor(
    private builder: FormBuilder,
    public dialogRef: MatDialogRef<EditPoleComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private poleService: PoleService) {
      this.name = data.pole.name;
      this.id = data.pole.id;
      this.editPoleForm = this.builder.group({
        name: new FormControl('')
      });
    }

    getPole() {
      this.poleService.getPole(this.id).subscribe();
    }

    onPoleEditFormSubmit(form) {
      this.name = form;
      this.poleService.updatePole(this.id, form).subscribe();
      this.onClose();
    }

    onClose() {
      this.dialogRef.close();
    }

    ngOnInit() {
      this.getPole();
  }

}
