import { Component, OnInit, ViewChild, Input, OnChanges, SimpleChanges } from '@angular/core';
import { UserService } from 'src/app/shared/services/users.service';
import { MatSort, MatTableDataSource } from '@angular/material';
import { User } from 'src/app/shared/models/user';
import { Pole } from 'src/app/shared/models/pole';

@Component({
  selector: 'app-details-pole',
  templateUrl: './details-pole.component.html',
  styleUrls: ['./details-pole.component.scss']
})
export class DetailsPoleComponent implements OnChanges {
  users: User[];
  filterUsers: User[] = [];
  poles: Pole[] = [];

  @Input() pole: Pole;
  id: number;

  dataSource = new MatTableDataSource(this.users);
  displayedColumns: string[] = ['prenom', 'nom'];

  @ViewChild(MatSort) sort: MatSort;

  constructor(private userService: UserService) {}

  ngOnChanges() {
    this.getUsers();

  }
  getUsers() {
    this.users = [];
    this.userService.getUsers()
    .subscribe(( userData) => {
      this.users = userData.data;
      this.dataSource = new MatTableDataSource(userData.data);
      this.dataSource.sort = this.sort;
      this.filterUsers = this.users.filter(u => u.pole === this.pole.id);

    });

  }
}
