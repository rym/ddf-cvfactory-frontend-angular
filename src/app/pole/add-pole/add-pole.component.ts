import { Component, OnInit, EventEmitter } from '@angular/core';
import { PoleService } from './../../shared/services/pole.service';
import { FormGroup, FormBuilder, FormControl, NgForm } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-pole',
  templateUrl: './add-pole.component.html',
  styleUrls: ['./add-pole.component.scss']
})
export class AddPoleComponent implements OnInit {
  addNewPoleForm: FormGroup;
  name: string;
  event: EventEmitter<any> = new EventEmitter();
  constructor(
    private builder: FormBuilder,
    public dialogRef: MatDialogRef<AddPoleComponent>,
    private poleService: PoleService) {
      this.addNewPoleForm = this.builder.group({
        name: new FormControl('', [])
      });
    }

    onPoleFormSubmit(form) {
      this.name = form.name;
      this.poleService.addPole(form).subscribe();
      this.onClose();
    }
    onClose() {
      this.dialogRef.close();
    }
    ngOnInit() {}

}
