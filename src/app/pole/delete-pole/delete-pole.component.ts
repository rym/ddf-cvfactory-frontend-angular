import { Component, EventEmitter, Inject, OnInit } from '@angular/core';
import { PoleService } from 'src/app/shared/services/pole.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Pole } from './../../shared/models/pole';

@Component({
  selector: 'app-delete-pole',
  templateUrl: './delete-pole.component.html',
  styleUrls: ['./delete-pole.component.scss']
})
export class DeletePoleComponent implements OnInit {

  event: EventEmitter<any> = new EventEmitter();
  id: string;
  constructor(
    public dialogRef: MatDialogRef<DeletePoleComponent>,
    private poleService: PoleService,
    @Inject(MAT_DIALOG_DATA) data) {
      this.id = data.pole.id;
  }
  getPole() {
    this.poleService.getPole(this.id).subscribe();
  }

  deletePole() {
    this.poleService.deletePole(this.id).subscribe();
    this.event.emit('OK');
    this.dialogRef.close();
  }

  onClose() {
    this.dialogRef.close();

  }
  ngOnInit() {
    this.getPole();
  }
}
