import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListPolesComponent } from './list-poles/list-poles.component';
import { AddPoleComponent } from './add-pole/add-pole.component';
import { DeletePoleComponent } from './delete-pole/delete-pole.component';
import { EditPoleComponent } from './edit-pole/edit-pole.component';
import { DetailsPoleComponent } from './details-pole/details-pole.component';


const routes: Routes = [
  { path: 'list', component: ListPolesComponent },
  { path: 'ajouterpole', component: AddPoleComponent },
  { path: 'modifierpole', component: EditPoleComponent },
  { path: 'supprimerpole', component: DeletePoleComponent },
  { path: 'detailspole', component: DetailsPoleComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class PoleRoutingModule { }

