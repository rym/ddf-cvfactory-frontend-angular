import { Component, OnInit } from '@angular/core';
import { PoleService } from './../../shared/services/pole.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AddPoleComponent } from '../add-pole/add-pole.component';
import { DeletePoleComponent } from '../delete-pole/delete-pole.component';
import { EditPoleComponent } from '../edit-pole/edit-pole.component';
import { Pole, PoleDto } from './../../shared/models/pole';

@Component({
  selector: 'app-list-poles',
  templateUrl: './list-poles.component.html',
  styleUrls: ['./list-poles.component.scss']
})
export class ListPolesComponent implements OnInit {
  poles: Pole[];
  selectedPole: Pole;

  constructor(public poleService: PoleService, private dialog: MatDialog) {}

  onSelect(pole: Pole): void {
    this.selectedPole = pole;
  }

  ngOnInit() {
    this.getPoles();
  }
  getPoles() {
    this.poles = [];
    this.poleService.getPoles().subscribe((poleData: PoleDto) => {
      this.poles = poleData.data;
    });
  }
  addNewPole(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '550px';
    const dialogRef = this.dialog.open(AddPoleComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(value => {
      this.getPoles();
    });
  }

  deletePole(pole: Pole): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '550px';
    dialogConfig.data = { pole };
    const dialogRef = this.dialog.open(DeletePoleComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(value => {
      this.getPoles();
    });
  }

  editPole(pole: Pole): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '550px';
    dialogConfig.data = { pole };
    const dialogRef = this.dialog.open(EditPoleComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(value => {
      this.getPoles();
    });
  }
}
