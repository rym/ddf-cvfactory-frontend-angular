import { NgModule } from '@angular/core';
import { PoleRoutingModule } from './pole-routing.module';
import { ListPolesComponent } from './list-poles/list-poles.component';
import { AddPoleComponent } from './add-pole/add-pole.component';
import { EditPoleComponent } from './edit-pole/edit-pole.component';
import { DeletePoleComponent } from './delete-pole/delete-pole.component';
import { SharedModule } from '../shared/shared-module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';
import { DetailsPoleComponent } from './details-pole/details-pole.component';

@NgModule({
  declarations: [
    ListPolesComponent,
    AddPoleComponent,
    EditPoleComponent,
    DeletePoleComponent,
    DetailsPoleComponent

  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PoleRoutingModule,
    SharedModule,
    ModalModule.forRoot()
  ],
  providers: [BsModalService],
  entryComponents:[AddPoleComponent, EditPoleComponent, DeletePoleComponent]
})
export class PoleModule { }
