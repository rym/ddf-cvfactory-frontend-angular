import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InscriptionComponent } from './authentification/inscription/inscription.component';
import { ConnectionComponent } from './authentification/connection/connection.component';
import { AuthGuard } from './shared/services/authGuard.service';
import { HomeComponent } from './home/home.component';
import { ProfilComponent } from './profil/profil.component';




const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'list', component: HomeComponent , canActivate: [AuthGuard] },
  { path: 'cv', loadChildren: './cv/cv-module#CvModule', canActivate: [AuthGuard] },
  { path: 'user', loadChildren: './user/user-module#UserModule', canActivate: [AuthGuard] },
  { path: 'pole', loadChildren: './pole/pole-module#PoleModule', canActivate: [AuthGuard] },
  { path: 'inscription', component: InscriptionComponent },
  { path: 'login', component: ConnectionComponent },
  { path: 'profil', component: ProfilComponent , canActivate: [AuthGuard] }
];
@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes )],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
