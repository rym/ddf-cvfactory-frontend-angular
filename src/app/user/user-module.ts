import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared-module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';
import { AddUserComponent } from './add-user/add-user.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { ListUserComponent } from './list-users/list-users.component';
import { DeleteUserComponent } from './delete-user/delete-user.component';
import { UserRoutingModule } from './user-routing.module';


@NgModule({
  declarations: [
    AddUserComponent,
    UpdateUserComponent,
    DeleteUserComponent,
    ListUserComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UserRoutingModule,
    SharedModule,
    ModalModule.forRoot()
],
  providers: [BsModalService],
  entryComponents: [AddUserComponent, UpdateUserComponent, DeleteUserComponent]
})
export class UserModule {}
