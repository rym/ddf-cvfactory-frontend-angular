import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListUserComponent } from './list-users/list-users.component';
import { AddUserComponent } from './add-user/add-user.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { DeleteUserComponent } from './delete-user/delete-user.component';




const routes: Routes = [
  { path: 'listUtilisateur', component: ListUserComponent },
  { path: 'ajouterUtilisateur', component: AddUserComponent },
  { path: 'modifierUtilisateur', component: UpdateUserComponent },
  { path: 'supprimerUtilisateur', component: DeleteUserComponent },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class UserRoutingModule { }
