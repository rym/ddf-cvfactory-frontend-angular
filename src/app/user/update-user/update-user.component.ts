import { Component, OnInit, EventEmitter, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, NgForm, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from './../../shared/services/users.service';
import { User } from './../../shared/models/user';
import { PoleService } from 'src/app/shared/services/pole.service';
import { Pole } from 'src/app/shared/models/pole';


@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit {
  editUserForm: FormGroup;
  user: User;
  nom: string;
  prenom: string;
  email: string;
  pole: Pole;
  poles: Pole[] = [];
  event: EventEmitter<any> = new EventEmitter();

  constructor(
    private builder: FormBuilder,
    public dialogRef: MatDialogRef<UpdateUserComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private userService: UserService,
    private poleService: PoleService) {
      this.user = data.user;
      this.nom = data.user.nom;
      this.prenom = data.user.prenom;
      this.email = data.user.email;
      this.pole = data.user.pole;
      this.editUserForm = this.builder.group({
        nom: [null, Validators.required],
        prenom: [null, Validators.required],
        pole: [null, Validators.required],
        email: [null, Validators.required]
        });
      this.editUserForm.get('nom').setValue(this.nom);
      this.editUserForm.get('prenom').setValue(this.prenom);
      this.editUserForm.get('email').setValue(this.email);
      this.editUserForm.get('pole').setValue(this.pole);
    }

  onUserFormSubmit(form) {
    this.nom = form.nom;
    this.prenom = form.prenom;
    this.email = form.email;
    this.pole = form.pole;
    this.userService.cleanObject(this.user);
    this.userService.addUser(form).subscribe();
    this.onClose();
  }

  onClose() {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.poleService.getPoles().subscribe(poleData => this.poles = poleData.data);
  }

}
