import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource, MatDialog, MatDialogConfig } from '@angular/material';
import { UserService } from '../../shared/services/users.service';
import { CvService } from 'src/app/shared/services/cv.service';
import { AddUserComponent } from '../add-user/add-user.component';
import { DeleteUserComponent } from '../delete-user/delete-user.component';
import { UpdateUserComponent } from '../update-user/update-user.component';
import { PoleService } from 'src/app/shared/services/pole.service';
import { forkJoin } from 'rxjs';
import { User } from 'src/app/shared/models/user';
import { Pole } from 'src/app/shared/models/pole';

@Component({
  selector: 'list-user',
  templateUrl: 'list-user.component.html',
  styleUrls: ['list-user.component.scss'],
})
export class ListUserComponent implements OnInit {
  id: number;
  i = 0;
  users: User[];
  poles: Pole[] = [];
  dataSource = new MatTableDataSource(this.users);
  displayedColumns: string[] = ['prenom', 'nom', 'pole', 'date', 'star'];

  @ViewChild(MatSort) sort: MatSort;

  constructor(private userService: UserService,
              private dialog: MatDialog, private poleService: PoleService) { }


  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.users = [];
    forkJoin(
      this.userService.getUsers(),
      this.poleService.getPoles()
    )
    .subscribe(( [userData, poleData] ) => {
      this.users = userData.data;
      this.dataSource = new MatTableDataSource(userData.data);
      this.dataSource.sort = this.sort;
      this.poles = poleData.data;
      this.populatePoleName();
    });

  }
  populatePoleName() {
    for (const user of this.users) {
      const pole =  this.poles.find(p => p.id === user.pole);
      if (pole) {
        user.poleName = pole.name;
      }
    }
  }

  addNewUser() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '550px';
    const dialogRef = this.dialog.open(AddUserComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(value => {
      this.getUsers();
    });
  }

  editUser(userEmail, userValue) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '550px';
    dialogConfig.data = {
      email: userEmail,
      user: userValue
    };
    const dialogRef = this.dialog.open(UpdateUserComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(value => {
      this.getUsers();
    });
  }

  deleteUser(userEmail: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '550px';
    dialogConfig.data = {
      email: userEmail,
    };
    const dialogRef = this.dialog.open(DeleteUserComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(value => {
      this.getUsers();
    });
  }
}
