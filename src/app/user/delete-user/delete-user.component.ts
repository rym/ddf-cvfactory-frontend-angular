import { Component, OnInit, EventEmitter, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from 'src/app/shared/services/users.service';
import { User } from './../../shared/models/user';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.scss']
})
export class DeleteUserComponent implements OnInit {
  user: User;
  email: string;
  event: EventEmitter<any> = new EventEmitter();
  constructor(public dialogRef: MatDialogRef<DeleteUserComponent>,
              private userService: UserService,
              @Inject(MAT_DIALOG_DATA) data) {
                this.email = data.email;
               }

  deleteUser() {
    this.userService.deleteUser(this.email).subscribe();
    this.event.emit('OK');
    this.dialogRef.close();
  }
  onClose() {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.getUser();
  }
  getUser() {
    this.userService.getUser(this.email).subscribe();
  }
}
