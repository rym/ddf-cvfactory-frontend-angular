import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, NgForm } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { UserService } from 'src/app/shared/services/users.service';
import { PoleService } from 'src/app/shared/services/pole.service';
import { Pole } from 'src/app/shared/models/pole';
import { UserRegistrationService } from 'src/app/shared/services/cognito/user-registration.service';
import { AlertService } from 'src/app/shared/services/alert.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  addNewUserForm: FormGroup;
  user;
  poles: Pole[] = [];
  event: EventEmitter<any> = new EventEmitter();
  errorMessage: string;
  constructor(
    private builder: FormBuilder,
    public dialogRef: MatDialogRef<AddUserComponent>,
    private userService: UserService,
    private poleService: PoleService,
    private userRegistrationService: UserRegistrationService,
    private alertService: AlertService
  ) {
    this.addNewUserForm = this.builder.group({
      nom: new FormControl('', []),
      prenom: new FormControl('', []),
      pole: new FormControl('', []),
      email: new FormControl('', [])
    });
  }

  onUserFormSubmit(form: NgForm) {
    this.userService.cleanObject(form);

    this.userRegistrationService.register({password: 'Password01!', ...this.addNewUserForm.value}, this);
    this.userService.addUser(form).subscribe();
    this.onClose();
  }

  onClose() {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.getPoles();
  }
  getPoles() {
    this.poleService.getPoles().subscribe(res => {
      this.poles = res.data;
    });
  }
  cognitoCallback(message: string, result: any) {
    if (message != null) { // error
        this.errorMessage = message;
    } else { // success
        this.alertService.success('Registration successful', true);
    }
  }
}
