import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Cv } from 'src/app/shared/models/cv';
import { CvService } from 'src/app/shared/services/cv.service';
import { Technologie } from 'src/app/shared/models/technologie';


@Component({
  selector: 'technologie',
  templateUrl: 'technologie.component.html',
  styleUrls: ['technologie.component.scss']
})
export class TechnologieComponent implements OnInit {
  technologieForm: FormGroup;
  cv: Cv;
  errorMessage: string;

  constructor(private router: Router, private cvService: CvService, private formBuilder: FormBuilder) {
  }
  ngOnInit() {
    this.technologieForm = this.formBuilder.group({
      langageP: ['', Validators.required],
      langageS: ['', Validators.required],
      baseDonnee: ['', Validators.required],
      methodologie: ['', Validators.required],
      framework: ['', Validators.required],
      git: ['', Validators.required],
      build: ['', Validators.required]
    });
    this.getCv();
  }


  getCv(): void {
    this.cvService.cv$
      .subscribe(
        (cv: Cv) => this.displayCv(cv),
        (error: any) => this.errorMessage = error as any
      );
  }

  displayCv(cv: Cv) {

    if (this.technologieForm) {
      this.technologieForm.reset();
    }

    if (!cv) {
      return;
    }

    this.cv = cv;
    if (!this.cv.technologie) {
      this.cv.technologie = new Technologie();
    }

    this.technologieForm.patchValue({
      langageP: this.cv.technologie.langageP,
      langageS: this.cv.technologie.langageS,
      baseDonnee: this.cv.technologie.baseDonnee,
      methodologie: this.cv.technologie.methodologie,
      framework: this.cv.technologie.methodologie,
      git: this.cv.technologie.methodologie,
      build: this.cv.technologie.methodologie
    });


  }

  suivant(): void {
    this.updateCurentCv();
    this.router.navigate([`cv/competence`]);
  }
  precedent(): void {
    this.updateCurentCv();
    this.router.navigate([`cv/certification`]);
  }

  updateCurentCv() {
    const technologie = this.technologieForm.value;
    this.cvService.cleanList(technologie);
    this.cv.technologie = this.technologieForm.value;
    this.cvService.updateCurrentValue(this.cv);
  }
  sauvegardeCv() {
    this.updateCurentCv();
    this.cvService.updateCv(this.cvService.cv$.value, this.cvService.user.email)
      .subscribe();
  }

}
