import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { CvService } from 'src/app/shared/services/cv.service';
import { Cv } from 'src/app/shared/models/cv';
import { Experience } from 'src/app/shared/models/experiences';


@Component({
  selector: 'experience',
  templateUrl: 'experience.component.html',
  styleUrls: ['experience.component.scss']
})
export class ExperienceComponent implements OnInit {
  experienceForm: FormGroup;
  cv: Cv;
  errorMessage: string;
  anneeCourante = false;

  constructor(private router: Router, private cvService: CvService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.experienceForm = this.formBuilder.group({
      experiences: this.formBuilder.array([]),
    });
    this.getCv();

      }

  getDateExpereince() {
    if (this.anneeCourante === true) {
      this.experienceForm.value.experiences.anneesFormationfin = null;
      this.experienceForm.value.experiences.moisExperiencedeb = null;
    } else {
      this.experienceForm.value.experiences.anneesFormationfin = '';
      this.experienceForm.value.experiences.moisExperiencefin = '';
    }
  }

  getCv(): void {
    this.cvService.cv$
      .subscribe(
        (cv: Cv) => this.displayCv(cv),
        (error: any) => this.errorMessage = error as any
      );
  }

  displayCv(cv: Cv) {
    if (this.experienceForm) {
      this.experienceForm.reset();
    }
    if (!cv) {
      this.router.navigate(['/list']);
    }
    this.cv = cv;
    const experiences = this.experienceForm.get('experiences') as FormArray;
    experiences.controls = [];
    if (!this.cv.experiences || this.cv.experiences.length === 0) {
      this.cv.experiences = new Array<Experience>();
      experiences.push(this.initExperience());
    } else {
      this.cv.experiences.forEach(() => experiences.push(this.initExperience()));
      experiences.patchValue(this.cv.experiences);
    }
  }

  initExperience(): FormGroup {
    return this.formBuilder.group({
      anneesExperiencedeb: ['', Validators.required],
      anneesExperiencefin: [''],
      moisExperiencedeb: ['', Validators.required],
      moisExperiencefin: [''],
      anneeCourante: [''],
      entreprise: ['', Validators.required],
      poste: ['', Validators.required],
      projet: ['', Validators.required],
      technique: ['', Validators.required],
      description: ['', Validators.required],
    });
  }
  addExperience() {
    const experiencesFormaArray = this.experienceForm.get('experiences') as FormArray;
    experiencesFormaArray.push(this.initExperience());
  }

  getExperience() {
    const experiencesFormArray = this.experienceForm.get('experiences') as FormArray;
    return experiencesFormArray.controls;
  }

  precedent(): void {
    this.updateCurentCv();
    this.router.navigate([`cv/competence`]);
  }

  suivant(): void {
    this.updateCurentCv();
    this.router.navigate([`cv/langue`]);
  }

  updateCurentCv() {
    const experiencesFormArray = this.experienceForm.get('experiences') as FormArray;
    const experiences = experiencesFormArray.value;
    this.cvService.cleanList(experiences);
    this.cv.experiences = experiences;
    this.cvService.updateCurrentValue(this.cv);
  }


  sauvegardeCv() {
    this.updateCurentCv();
    this.cvService.updateCv(this.cvService.cv$.value, this.cvService.user.email).subscribe();
  }

}

