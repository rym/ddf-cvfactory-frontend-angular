import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { CvService } from 'src/app/shared/services/cv.service';
import { Cv } from 'src/app/shared/models/cv';
import { Loisir } from 'src/app/shared/models/loisirs';


@Component({
  selector: 'loisir',
  templateUrl: 'loisir.component.html',
  styleUrls: ['loisir.component.scss']
})

export class LoisirComponent implements OnInit {
  loisirForm: FormGroup;
  cv: Cv;
  errorMessage: string;

  constructor(private router: Router, private cvService: CvService, private formBuilder: FormBuilder) { }
  ngOnInit() {
    this.loisirForm = this.formBuilder.group({
      loisirs: this.formBuilder.array([])
    });
    this.getCv();
  }

  getCv(): void {
    this.cvService.cv$
      .subscribe(
        (cv: Cv) => this.displayCv(cv),
        (error: any) => this.errorMessage = error as any
      );
  }

  displayCv(cv: Cv) {
    if (this.loisirForm) {
      this.loisirForm.reset();
    }
    if (!cv) {
      this.router.navigate(['/list']);
    }
    this.cv = cv;
    const loisirs = this.loisirForm.get('loisirs') as FormArray;
    loisirs.controls = [];
    for (let i = 0; i < 5; i++) {
      loisirs.push(this.initLoisir());
    }
    if (!this.cv.loisirs || this.cv.loisirs.length === 0) {
      this.cv.loisirs = new Array<Loisir>();
    } else {
      loisirs.patchValue(this.cv.loisirs);
    }

  }

  initLoisir(): FormGroup {
    return this.formBuilder.group({
      loisir: ['']
    });
  }

  getLoisirs() {
    const loisirsFormArray = this.loisirForm.get('loisirs') as FormArray;
    return loisirsFormArray.controls;
  }

  precedent(): void {
    this.updateCurentCv();
    this.router.navigate([`cv/experience`]);
  }

  suivant(): void {
    this.updateCurentCv();
    this.router.navigate([`cv/loisir`]);
  }

  updateCurentCv() {
    const loisirsFormArray = this.loisirForm.get('loisirs') as FormArray;
    const loisirs = loisirsFormArray.value;
    this.cvService.cleanList(loisirs);
    this.cv.loisirs = loisirs;
    this.cvService.updateCurrentValue(this.cv);
  }
  sauvegardeCv() {
    this.updateCurentCv();
    this.cvService.updateCv(this.cvService.cv$.value, this.cvService.user.email)
      .subscribe();
  }

}
