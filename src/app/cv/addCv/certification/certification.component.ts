import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CvService } from 'src/app/shared/services/cv.service';
import { Cv } from 'src/app/shared/models/cv';
import { Subscription } from 'rxjs';
import { Certification } from 'src/app/shared/models/certifications';


@Component({
  selector: 'certification',
  templateUrl: 'certification.component.html',
  styleUrls: ['certification.component.scss']
})
export class CertificationComponent implements OnInit {
  certificationForm: FormGroup;
  certifications: FormArray;
  id: number;
  cv: Cv | undefined;
  private sub: Subscription;
  errorMessage: string;
  userEmail: string;

  constructor(private route: ActivatedRoute, private router: Router, private cvService: CvService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.certificationForm = this.formBuilder.group({
      certifications: this.formBuilder.array([]),
    });
    this.getCv();
  }

  getCv(): void {
      this.cvService.cv$
        .subscribe(
          (cv: Cv) => this.displayCv(cv),
          (error: any) => this.errorMessage = error as any
        );
  }

  displayCv(cv: Cv) {
    if (this.certificationForm) {
      this.certificationForm.reset();
    }

    if (!cv) {
      this.router.navigate(['/list']);
    }
    this.cv = cv;
    const certifications = this.certificationForm.get('certifications') as FormArray;
    certifications.controls = [];
    if (!this.cv.certifications || this.cv.certifications.length === 0) {
      this.cv.certifications = new Array<Certification>();
      certifications.push(this.initCertification());
    } else {
      this.cv.certifications.forEach(() => certifications.push(this.initCertification()));
      certifications.patchValue(this.cv.certifications);
    }
  }

  initCertification(): FormGroup {
    return this.formBuilder.group({
      anneescertif: '',
      organisme: '',
      certif: ''
    });
  }
  addCertification() {
    const certificationsFormaArray = this.certificationForm.get('certifications') as FormArray;
    certificationsFormaArray.push(this.initCertification());
  }

  getCertification(form) {
    const certificationsFormArray = this.certificationForm.get('certifications') as FormArray;
    return certificationsFormArray.controls;
  }

  precedent(): void {
    this.updateCurentCv();
    this.router.navigate([`cv/formation`]);
  }

  suivant(): void {
    this.updateCurentCv();
    this.router.navigate([`cv/technologie`]);
  }

  updateCurentCv() {
    const certificationsFormaArray = this.certificationForm.get('certifications') as FormArray;
    const certifications = certificationsFormaArray.value;
    this.cvService.cleanList(certifications);
    this.cv.certifications = this.certificationForm.value.certifications;
    this.cvService.updateCurrentValue(this.cv);
  }
  sauvegardeCv() {
    this.updateCurentCv();
    this.cvService.updateCv(this.cvService.cv$.value, this.cvService.user.email)
      .subscribe();
  }

}
