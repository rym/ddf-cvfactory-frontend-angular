import { Component, OnInit } from '@angular/core';

import { Label, SingleDataSet } from 'ng2-charts';
import { ChartType } from 'chart.js';
import { CvService } from 'src/app/shared/services/cv.service';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-dialog-content',
  templateUrl: './dialog-content.component.html',
  styleUrls: ['./dialog-content.component.scss']
})
export class DialogContentComponent implements OnInit {
  cv;
  user;
  showChart: boolean;
  errorMessage: string;
  competenceLabel: Label[] = [];
  competenceNote: SingleDataSet = [];
  public polarAreaLegend = true;
  public polarAreaChartType: ChartType = 'polarArea';
  public chartType = 'polarArea';

  public chartColors: Array<any> = [
    {
      backgroundColor: ['#bde4a7', '#fcd7ad', '#fffd98', '#aee5d8', '#fddadf'],
      borderWidth: 0
    }
  ];

  public chartOptions: any = {
    responsive: true,
    maintainAspectRatio: true,
    legend: {
      position: 'right',
      labels: {
        boxWidth: 70,
        fontSize: 15,
        fontFamily: 'Roboto'
      }
    },
    scale: {
      ticks: {
        callback() {
          return '';
        }
      }
    }
  };
  constructor(private cvService: CvService) {}

  ngOnInit() {
    this.cv = this.cvService.cv$.value;
    this.user = this.cvService.user;
    this.cv.competences.mandatoryCompetences.forEach((competence, i) => {
      this.competenceLabel[i] = competence.label;
      this.competenceNote[i] = competence.note;
    });

    this.showChart = true;
  }

  isualisationCv() {
    this.cvService.cv$.value;
  }
  public printDiv() {
    const data = document.getElementById('contentToConvert');
    html2canvas(data).then(canvas => {
      const imgWidth = 158;
      const pageHeight = 185;
      const imgHeight = canvas.height * imgWidth / canvas.width;
      const heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('./assets/images/Logo3x-ConvertImage.png');
      const pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
      const position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
      pdf.save('MYPdf.pdf'); // Generated PDF
  });
  }


}
