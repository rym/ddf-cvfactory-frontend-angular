import { Component } from '@angular/core';
import {MatDialog} from '@angular/material';
import {DialogContentComponent} from './dialog-content/dialog-content.component';

@Component({
  selector: 'app-visualisationCv',
  templateUrl: './visualisationCv.component.html',
  styleUrls: ['./visualisationCv.component.scss']
})
export class VisualisationCvComponent {

  constructor(public dialog: MatDialog) { }

  openDialog(): void {
    this.dialog.open(DialogContentComponent, {
      height: '500%',
      width: '900%'
    });
  }
}
