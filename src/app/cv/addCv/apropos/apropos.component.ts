import { Component, OnInit } from '@angular/core';
import { CvService } from '../../../shared/services/cv.service';
import { Router } from '@angular/router';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { Cv } from '../../../shared/models/cv';
import { Apropos } from 'src/app/shared/models/apropos';

@Component({
  selector: 'apropos',
  templateUrl: 'apropos.component.html',
  styleUrls: ['./apropos.component.scss'],
  providers: []
})
export class AproposComponent implements OnInit {
  aproposForm: FormGroup;
  cv: Cv;
  errorMessage: string;
  titre = new FormControl();
  etat: string;
  constructor(
    private router: Router,
    private cvService: CvService,
    private formBuilder: FormBuilder
  ) {}

  etatCv(cvValids) {
    for (const cvValid of cvValids) {
      if (cvValid.isValid) {
        this.etat = 'complet';
      } else {
        this.etat = 'incomplet';
      }
    }
}

  ngOnInit() {
    this.aproposForm = this.formBuilder.group({
      titre: new FormControl("", Validators.required),
      anexperience: new FormControl("", Validators.required),
      description: new FormControl("", Validators.required)
    });
    this.getCv();
    this.cvService.cvValid$.subscribe(cvValids => {
      this.etatCv(cvValids);
    });
  }

  getCv(): void {
    this.cvService.cv$.subscribe(
      (cv: Cv) => this.displayCv(cv),
      (error: any) => (this.errorMessage = error as any)
    );
  }

  displayCv(cv: Cv) {
    if (this.aproposForm) {
      this.aproposForm.reset();
    }
    if (!cv) {
      return;
    }
    this.cv = cv;
    if (!this.cv.apropos) {
      this.cv.apropos = new Apropos();
    }
    this.aproposForm.patchValue({
      titre: this.cv.apropos.titre,
      anexperience: this.cv.apropos.anexperience,
      description: this.cv.apropos.description
    });
  }

  suivant(): void {
    this.updateCurentCv();
    this.router.navigate([`cv/formation`]);
  }

  updateCurentCv() {
    const apropos = this.aproposForm.value;
    this.cvService.cleanObject(apropos);
    this.cv.apropos = this.aproposForm.value;
    this.cvService.updateCurrentValue(this.cv);
  }

  sauvegardeCv() {
    this.updateCurentCv();
    this.cvService.updateCv(this.cvService.cv$.value, this.cvService.user.email).subscribe();
  }
}
