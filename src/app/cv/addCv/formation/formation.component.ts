import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { CvService } from 'src/app/shared/services/cv.service';
import { Cv } from 'src/app/shared/models/cv';
import { Formation } from 'src/app/shared/models/formations';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'formation',
  templateUrl: 'formation.component.html',
  styleUrls: ['formation.component.scss'],
  providers: [DatePipe],
})
export class FormationComponent implements OnInit {
  formationForm: FormGroup;
  formations: FormArray;
  cv: Cv;
  errorMessage: string;
  anneeCourante = false;
  error: any = { isError: false, errorMessage: '' };

  constructor(private router: Router, private cvService: CvService, private formBuilder: FormBuilder) { }

  getDateFormation() {
    if (this.anneeCourante === true) {
      this.formationForm.value.formations.anneesFormationfin = null;
    } else {
      this.formationForm.value.formations.anneesFormationfin = '';
    }
  }

  validationDate() {
    if (this.formationForm.value.formations.anneesFormationdeb > this.formationForm.value.formations.anneesFormationfin) {
      return this.error;
    }
  }

  ngOnInit() {
    this.formationForm = this.formBuilder.group({
      formations: this.formBuilder.array([]),
    });
    this.getCv();
  }

  getCv(): void {
      this.cvService.cv$
        .subscribe(
          (cv: Cv) => this.displayCv(cv),
          (error: any) => this.errorMessage = error as any
        );
  }

  displayCv(cv: Cv) {
    if (this.formationForm) {
      this.formationForm.reset();
    }
    if (!cv) {
      this.router.navigate(['/list']);
    }
    this.cv = cv;
    const formations = this.formationForm.get('formations') as FormArray;
    formations.controls = [];
    if (!this.cv.formations || this.cv.formations.length === 0) {
      this.cv.formations = new Array<Formation>();
      formations.push(this.initFormation());
    } else {
      this.cv.formations.forEach(() => formations.push(this.initFormation()));
      formations.patchValue(this.cv.formations);
    }
  }

  initFormation(): FormGroup {
    return this.formBuilder.group({
      anneesFormationdeb: ['', Validators.required],
      anneesFormationfin: [''],
      anneeCourante: [''],
      ecole: ['', Validators.required],
      diplome: ['', Validators.required]
    });
  }

  addFormation() {
    const formationsFormaArray = this.formationForm.get('formations') as FormArray;
    formationsFormaArray.push(this.initFormation());
  }

  getFormation(form) {
    const formationsFormArray = this.formationForm.get('formations') as FormArray;
    return formationsFormArray.controls;
  }

  precedent(): void {
    this.updateCurentCv();
    this.router.navigate([`cv/apropos`]);
  }

  suivant(): void {
    this.updateCurentCv();
    this.router.navigate([`cv/certification`]);
  }

  updateCurentCv() {
    const formationsFormArray = this.formationForm.get('formations') as FormArray;
    const formations = formationsFormArray.value;
    this.cvService.cleanList(formations);
    this.cv.formations = this.formationForm.value.formations;
    this.cvService.updateCurrentValue(this.cv);
    this.cvService.stateCv(this.cvService.cv$.value);
  }

  sauvegardeCv() {
    this.updateCurentCv();
    this.cvService.updateCv(this.cvService.cv$.value, this.cvService.user.email)
      .subscribe();
  }

}
