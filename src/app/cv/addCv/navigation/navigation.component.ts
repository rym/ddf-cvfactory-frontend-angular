import { Component, OnInit, Input } from '@angular/core';
import { CvService } from 'src/app/shared/services/cv.service';


@Component({
  selector: 'cv-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  @Input() name: string;
  rubriques = [
    { name: 'à propos', key: 'apropos', routerLink: [`/cv/apropos`], iconClass: 'iconCross' },
    { name: 'formation', key: 'formations', routerLink: [`/cv/formation`], iconClass: 'iconCross' },
    { name: 'certifications', key: 'certification', routerLink: [`/cv/certification`] },
    { name: 'technologies', key: 'technologie', routerLink: [`/cv/technologie`], iconClass: 'iconCross' },
    { name: 'compétences', key: 'competence', routerLink: [`/cv/competence`], iconClass: 'iconCross' },
    { name: 'expériences', key: 'experience', routerLink: [`/cv/experience`], iconClass: 'iconCross' },
    { name: 'langues', key: 'langue', routerLink: [`/cv/langue`], iconClass: 'iconCross' },
    { name: 'loisirs', key: 'loisir', routerLink: [`/cv/loisir`] },
  ];
  constructor(private cvService: CvService) { }

  ngOnInit() {
    this.cvService.cvValid$.subscribe(cvValids => {
      this.stateRubrique(cvValids);
    });
  }

  stateRubrique(cvValids) {
    for (const rubrique of this.rubriques) {
      for (const cvValid of cvValids) {
        if (rubrique.key === cvValid.page && cvValid.isValid) {
          rubrique.iconClass = 'iconCheck';
        }
      }

    }


  }
}
