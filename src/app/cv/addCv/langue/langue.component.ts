import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { Cv } from 'src/app/shared/models/cv';
import { CvService } from 'src/app/shared/services/cv.service';
import { Langue } from 'src/app/shared/models/langues';


export interface langue {
  value: string;
  viewValue: string;
}
export interface niveau {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'langue',
  templateUrl: 'langue.component.html',
  styleUrls: ['langue.component.scss']
})
export class LangueComponent implements OnInit {
  langueForm: FormGroup;
  cv: Cv;
  errorMessage: string;


  langues: langue[] = [
    { value: 'français', viewValue: 'français' },
    { value: 'anglais', viewValue: 'anglais' },
    { value: 'arabe', viewValue: 'arabe' },
    { value: 'espagnol', viewValue: 'espagnol' },
    { value: 'italien', viewValue: 'italien' }

  ];
  niveaux: niveau[] = [
    { value: 'natal', viewValue: 'langue natale' },
    { value: 'bilingue', viewValue: 'bilingue' },
    { value: 'conversationnel', viewValue: 'conversationnel' },
    { value: 'notion', viewValue: 'notion' }

  ];

  constructor(private router: Router, private cvService: CvService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.langueForm = this.formBuilder.group({
      langues: this.formBuilder.array([])
    });
    this.getCv();
  }

  getCv(): void {
    this.cvService.cv$
      .subscribe(
        (cv: Cv) => this.displayCv(cv),
        (error: any) => this.errorMessage = error as any
      );
  }

  displayCv(cv: Cv) {
    if (this.langueForm) {
      this.langueForm.reset();
    }
    if (!cv) {
      this.router.navigate(['/list']);
    }
    this.cv = cv;
    const langues = this.langueForm.get('langues') as FormArray;
    langues.controls = [];
    for (let i = 0; i < 5; i++) {
      langues.push(this.initLangue());
    }
    if (!this.cv.langues || this.cv.langues.length === 0) {
      this.cv.langues = new Array<Langue>();
    } else {
      langues.patchValue(this.cv.langues);
    }

  }

  initLangue(): FormGroup {
    return this.formBuilder.group({
      niveau: ['', Validators.required],
      langue: ['', Validators.required],
      commentaire: ['']
    });
  }

  getLangues() {
    const languesFormArray = this.langueForm.get('langues') as FormArray;
    return languesFormArray.controls;
  }

  precedent(): void {
    this.updateCurentCv();
    this.router.navigate([`cv/experience`]);
  }

  suivant(): void {
    this.updateCurentCv();
    this.router.navigate([`cv/loisir`]);
  }

  updateCurentCv() {
    const languesFormArray = this.langueForm.get('langues') as FormArray;
    const langues = languesFormArray.value;
    this.cvService.cleanList(langues);
    this.cv.langues = langues;
    this.cvService.updateCurrentValue(this.cv);
  }

  sauvegardeCv() {
    this.updateCurentCv();
    this.cvService.updateCv(this.cvService.cv$.value, this.cvService.user.email)
      .subscribe();
  }

}
