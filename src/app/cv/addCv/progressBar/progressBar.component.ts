import { Component, OnInit } from '@angular/core';
import { CvService } from 'src/app/shared/services/cv.service';

@Component({
  selector: 'app-progressBar',
  templateUrl: './progressBar.component.html',
  styleUrls: ['./progressBar.component.scss']
})
export class stateProgressBarComponent implements OnInit {
  color = 'warn';
  mode = 'determinate';

  constructor(private cvService: CvService) {}

  stateProgressBar() {
    this.cvService.cv$.subscribe(cv => {
      this.cvService.stateCv(cv).subscribe((cvValids) => {
        let nbValid = 0;
        nbValid = cvValids.filter(cv => cv.isValid).length;
        if (cvValids.length) {
          this.cvService.value = Math.round((nbValid / cvValids.length) * 100);
        } else {
          this.cvService.value = 0;
        }
      });
    });

  }

  ngOnInit() {
    this.stateProgressBar();
  }


}

