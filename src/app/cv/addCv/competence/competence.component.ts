import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { CvService } from 'src/app/shared/services/cv.service';
import { Cv } from 'src/app/shared/models/cv';
import { Competences } from 'src/app/shared/models/competence';
import { ChartType } from 'chart.js';
import { SingleDataSet, Label } from 'ng2-charts';


@Component({
  selector: 'competence',
  templateUrl: 'competence.component.html',
  styleUrls: ['competence.component.scss']
})
export class CompetenceComponent implements OnInit {
  competenceForm: FormGroup;
  cv: Cv;
  showChart: boolean;
  errorMessage: string;
  competenceLabel: Label[] = [];
  competenceNote: SingleDataSet = [];
  public polarAreaLegend = true;
  public polarAreaChartType: ChartType = 'polarArea';
  public chartType = 'polarArea';

  public chartColors: Array<any> = [
    {
      backgroundColor: [
        '#bde4a7',
        '#fcd7ad',
        '#fffd98',
        '#aee5d8',
        '#fddadf'
      ],
      borderWidth: 0
    }
  ];

  public chartOptions: any = {
    responsive: true,
    maintainAspectRatio: true,
    legend: {
      position: 'right',
      labels: {
        boxWidth: 70,
        fontSize: 15,
        fontFamily: 'Roboto'
      }
    },
    scale: {
      ticks: {
        callback() { return ''; }
      }
    }
  };

  constructor(private router: Router, private cvService: CvService, private formBuilder: FormBuilder) { }
  ngOnInit() {
    this.competenceForm = this.formBuilder.group({
      mandatoryCompetences: this.formBuilder.array([]),
      otherCompetences: this.formBuilder.array([])
    });
    this.getCv();

    this.competenceForm.valueChanges.subscribe(valueChanges => {

      this.showChart = false;
      valueChanges.mandatoryCompetences.forEach((competence, i) => {
        this.competenceLabel[i] = competence.label;
        this.competenceNote[i] = competence.note;
      });
      setTimeout(() => this.showChart = true, 1);
    });
    this.showChart = true;
  }


  getCv(): void {
    this.cvService.cv$
      .subscribe(
        (cv: Cv) => this.displayCv(cv),
        (error: any) => this.errorMessage = error as any
      );
  }


  displayCv(cv: Cv) {
    if (this.competenceForm) {
      this.competenceForm.reset();
    }
    if (!cv) {
      this.router.navigate(['/list']);
    }
    this.cv = cv;
    const mandatoryCompetences = this.competenceForm.get('mandatoryCompetences') as FormArray;
    mandatoryCompetences.controls = [];
    for (let i = 0; i < 5; i++) {
      mandatoryCompetences.push(this.initCompetence());
    }
    if (!this.cv.competences) {
      this.cv.competences = new Competences();
    } else {
      mandatoryCompetences.patchValue(this.cv.competences.mandatoryCompetences);
    }
    const mandatories = this.cv.competences.mandatoryCompetences;
    this.competenceLabel = mandatories.map(mandatory => mandatory.label);
    this.competenceNote = mandatories.map(mandatory => mandatory.note);

    const otherCompetences = this.competenceForm.get('otherCompetences') as FormArray;
    otherCompetences.controls = [];
    for (let i = 0; i < 5; i++) {
      otherCompetences.push(this.initOtherCompetence());
    }
    if (!this.cv.competences) {
      this.cv.competences = new Competences();
    } else {
      otherCompetences.patchValue(this.cv.competences.otherCompetences);
    }
  }

  initCompetence(): FormGroup {
    return this.formBuilder.group({
      label: ['', Validators.required],
      note: ['', Validators.required]
    });
  }
  initOtherCompetence(): FormGroup {
    return this.formBuilder.group({
      label: ''
    });
  }

  getMandatoryCompetence() {
    const mandatoryCompetencesFormArray = this.competenceForm.get('mandatoryCompetences') as FormArray;
    return mandatoryCompetencesFormArray.controls;
  }
  getOtherCompetence() {
    const otherCompetencesFormArray = this.competenceForm.get('otherCompetences') as FormArray;
    return otherCompetencesFormArray.controls;
  }
  precedent(): void {
    this.updateCurentCv();
    this.router.navigate([`cv/technologie`]);
  }

  suivant(): void {
    this.updateCurentCv();
    this.router.navigate([`cv/experience`]);
  }

  updateCurentCv() {
    const mandatoryCompetencesFormArray = this.competenceForm.get('mandatoryCompetences') as FormArray;
    const mandatoryCompetences = mandatoryCompetencesFormArray.value;
    this.cvService.cleanList(mandatoryCompetences);
    const otherCompetencesFormArray = this.competenceForm.get('otherCompetences') as FormArray;
    const otherCompetences = otherCompetencesFormArray.value;
    this.cvService.cleanList(otherCompetences);
    this.cv.competences = this.competenceForm.value;
    this.cvService.updateCurrentValue(this.cv);
    this.cvService.stateCv(this.cvService.cv$.value);
  }

  sauvegardeCv() {
    this.updateCurentCv();
    this.cvService.updateCv(this.cvService.cv$.value, this.cvService.user.email)
      .subscribe();
  }
}
