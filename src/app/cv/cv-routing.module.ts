import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AproposComponent } from './addCv/apropos/apropos.component';
import { FormationComponent } from './addCv/formation/formation.component';
import { CertificationComponent } from './addCv/certification/certification.component';
import { TechnologieComponent } from './addCv/technologie/technologie.component';
import { CompetenceComponent } from './addCv/competence/competence.component';
import { ExperienceComponent } from './addCv/experience/experience.component';
import { LangueComponent } from './addCv/langue/langue.component';
import { LoisirComponent } from './addCv/loisir/loisir.component';

const routes: Routes = [

  { path: 'detail/:email/:id', component: AproposComponent },
  { path: 'apropos', component: AproposComponent },
  { path: 'formation', component: FormationComponent },
  { path: 'certification', component: CertificationComponent },
  { path: 'technologie', component: TechnologieComponent },
  { path: 'competence', component: CompetenceComponent },
  { path: 'experience', component: ExperienceComponent },
  { path: 'langue', component: LangueComponent },
  { path: 'loisir', component: LoisirComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class CvRoutingModule { }

