import { VisualisationCvComponent } from './addCv/visualisationCv/visualisationCv.component';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AproposComponent } from './addCv/apropos/apropos.component';
import { FormationComponent } from './addCv/formation/formation.component';
import { CertificationComponent } from './addCv/certification/certification.component';
import { TechnologieComponent } from './addCv/technologie/technologie.component';
import { CompetenceComponent } from './addCv/competence/competence.component';
import { ExperienceComponent } from './addCv/experience/experience.component';
import { LangueComponent } from './addCv/langue/langue.component';
import { LoisirComponent } from './addCv/loisir/loisir.component';
import { SharedModule } from '../shared/shared-module';
import { CommonModule } from '@angular/common';
import { CvRoutingModule } from './cv-routing.module';
import {stateProgressBarComponent} from './addCv/progressBar/progressBar.component';

import { NavigationComponent } from './addCv/navigation/navigation.component';

@NgModule({
  declarations: [
    stateProgressBarComponent,
    AproposComponent,
    FormationComponent,
    CertificationComponent,
    TechnologieComponent,
    CompetenceComponent,
    ExperienceComponent,
    LangueComponent,
    LoisirComponent,
    VisualisationCvComponent,

    NavigationComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    CvRoutingModule
  ],

  providers: []
})
export class CvModule { }
