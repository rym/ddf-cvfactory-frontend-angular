import { Component } from '@angular/core';
import { UserService } from './shared/services/users.service';
import { UserLoginService } from './shared/services/cognito/user-login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cvfactory';
  constructor(public userLoginService: UserLoginService){

  }
}
