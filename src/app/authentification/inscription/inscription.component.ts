import { Component, OnInit, ElementRef, AfterViewInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/users.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { AlertService } from 'src/app/shared/services/alert.service';
import { UserRegistrationService } from 'src/app/shared/services/cognito/user-registration.service';
import { CognitoCallback } from 'src/app/shared/services/cognito/cognito.service';
import { Pole } from 'src/app/shared/models/pole';
import { PoleService } from 'src/app/shared/services/pole.service';
import { ErrorStateMatcher } from '@angular/material/core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})


export class InscriptionComponent implements OnInit, AfterViewInit, CognitoCallback  {
  poles: Pole[] = [];
  registerForm: FormGroup;
  matcher = new MyErrorStateMatcher();
  submitted = false;
  errorMessage: string;
  inscriptionSteps = [
    'firstStep',
    'secondStep',
    'finalStep'
  ];

  currentStep = '';
  constructor(private elementRef: ElementRef,
              private formBuilder: FormBuilder,
              private userService: UserService,
              private poleService: PoleService,
              private route: Router,
              private userRegistrationService: UserRegistrationService,
              private alertService: AlertService) {
                if (this.userService.currentUser) {
                  this.route.navigate(['/list']);
              }
              }

  ngOnInit() {
    this.currentStep = 'firstStep';
    this.registerForm = this.formBuilder.group({
      prenom: ['', Validators.required],
      nom: ['', Validators.required],
      email: ['', Validators.required],
      pole: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['']
    }, { validator: this.checkPasswords });
    this.getPoles() ;
  }
  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
  const pass = group.controls.password.value;
  const confirmPass = group.controls.confirmPassword.value;

  return pass === confirmPass ? null : { notSame: true };
}
  getPoles() {
    this.poleService.getPoles().subscribe(res => {
      this.poles = res.data;
    }
    );
  }
  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#114b5f';
  }
  nextStep() {
    this.currentStep = 'secondStep';
  }
  finalStep() {
    this.currentStep = 'finalStep';

  }

  get form() {
    return this.registerForm.controls;
  }
  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
        return;
    }
    this.errorMessage = null;
    this.userRegistrationService.register(this.registerForm.value, this);
    this.userService.addUser(this.registerForm.value).subscribe();
    this.finalStep();

  }

  cognitoCallback(message: string, result: any) {
    if (message != null) { // error
        this.errorMessage = message;
    } else { // success
        this.alertService.success('Registration successful', true);
    }
  }

}
