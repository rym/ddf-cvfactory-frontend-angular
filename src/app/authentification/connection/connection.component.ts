import { Component, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserLoginService } from 'src/app/shared/services/cognito/user-login.service';
import { UserService } from 'src/app/shared/services/users.service';
import { User } from './../../shared/models/user';

@Component({
  selector: 'app-connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.scss']
})
export class ConnectionComponent implements OnInit, AfterViewInit {
  loginForm: FormGroup;
  submitted = false;
  constructor(private elementRef: ElementRef,
              private formBuilder: FormBuilder,
              private userService: UserService,
              private router: Router,
              private userLoginService: UserLoginService ) { }
    ngAfterViewInit(){
      this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#114b5f';
   }
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
  });

  }
 // convenience getter for easy access to form fields
 get form() { return this.loginForm.controls; }

 onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }
    this.userLoginService.authenticate(this.form.email.value, this.form.password.value).then(
      () => {
        this.userService.getUser(this.form.email.value).subscribe(data => {
          this.userService.currentUser = data.user;

          this.router.navigateByUrl('/list');
          this.userLoginService.sucessToLogin = true;
        });
    }
    );
  }
  inscription(): void {
    this.router.navigate([`/inscription`]);
  }
}
