import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { UserService } from '../shared/services/users.service';
import { CvService } from 'src/app/shared/services/cv.service';
import { PoleService } from 'src/app/shared/services/pole.service';
import { forkJoin } from 'rxjs';
import { Pole, PoleDto } from '../shared/models/pole';
import { User, Role } from 'src/app/shared/models/user';
import { DialogContentComponent } from '../cv/addCv/visualisationCv/dialog-content/dialog-content.component';

@Component({
  selector: 'home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss']
})
export class HomeComponent implements OnInit {
  role = Role;
  cv;
  user;
  users: User[];
  poles: Pole[] = [];
  dataSource = new MatTableDataSource(this.users);
  displayedColumns: string[] = ['prenom', 'nom', 'pole', 'date', 'star'];

  @ViewChild(MatSort) sort: MatSort;

  constructor(public userService: UserService, private cvService: CvService, private poleService: PoleService, public dialog: MatDialog) {}

  ngOnInit() {
    this.getUsers();
    this.getPoles();
  }
  getPoles() {
    this.poles = [];
    this.poleService.getPoles().subscribe((poleData: PoleDto) => {
      this.poles = poleData.data;
    });
  }
  getUsers() {
    this.users = [];
    forkJoin(
      this.userService.getUsers(),
      this.poleService.getPoles()
    )
    .subscribe(( [userData, poleData] ) => {
      this.users = userData.data;
      this.dataSource = new MatTableDataSource(userData.data);
      this.dataSource.sort = this.sort;
      this.poles = poleData.data;
      this.populatePoleName();

    });

  }
  loadCv(user) {
    this.cvService.user = user;
    this.cvService.getCv(1, user.email).subscribe(cv => {
      if (cv) {
        this.cvService.cv$.next(cv);
        console.log('cv', cv);
      }
    });
  }
  loadProgressBar() {
    this.cvService.stateCv(this.cvService.cv$.getValue()).subscribe(cvValid => {
      if (cvValid) {
        this.cvService.cvValid$.next(cvValid);
      }
    });
  }

  public doFilter = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  populatePoleName() {
    for (const user of this.users) {
      const pole =  this.poles.find(p => p.id === user.pole);
      if (pole) {
        user.poleName = pole.name;
      }
    }
  }
  getCv(user): void {
    this.cvService.getCv(1, user.email).subscribe(cv => {
      if (cv) {
        this.cvService.cv$.next(cv);
        console.log('cv', cv);
      }
    });
  }
  openViewCvDialog(user: User): void {
    this.cvService.user = user;
    this.getCv(user);
    this.dialog.open(DialogContentComponent, {
      height: '500%',
      width: '900%'
    });
  }
}

