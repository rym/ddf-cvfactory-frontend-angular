import { Component, OnInit, EventEmitter, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { User, Role } from 'src/app/shared/models/user';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from 'src/app/shared/services/users.service';
import { Pole } from 'src/app/shared/models/pole';
import { PoleService } from 'src/app/shared/services/pole.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-edit-profil',
  templateUrl: './edit-profil.component.html',
  styleUrls: ['./edit-profil.component.scss']
})
export class EditProfilComponent implements OnInit {
  editUserForm: FormGroup;
  user: User;
  nom: string;
  prenom: string;
  email: string;
  role: Role;
  pole: Pole;
  poles: Pole[] = [];
  event: EventEmitter<any> = new EventEmitter();

  constructor(
    private builder: FormBuilder,
    public dialogRef: MatDialogRef<EditProfilComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private userService: UserService,
    private poleService: PoleService) {
      this.user = data.user;
      this.nom = data.user.nom;
      this.prenom = data.user.prenom;
      this.email = data.user.email;
      this.pole = data.user.pole;
      this.role = data.user.role;
      this.editUserForm = this.builder.group({
        nom: [null, Validators.required],
        prenom: [null, Validators.required],
        email: [null, Validators.required],
        pole: [null, Validators.required],
        });
      this.editUserForm.get('nom').setValue(this.nom);
      this.editUserForm.get('prenom').setValue(this.prenom);
      this.editUserForm.get('email').setValue(this.email);
      this.editUserForm.get('pole').setValue(this.pole);
    }

  onUserFormSubmit(form) {
    this.userService.currentUser.email = form.email;
    this.userService.currentUser.nom = form.nom;
    this.userService.currentUser.prenom = form.prenom;
    this.userService.addUser(form).subscribe();
    this.onClose();
  }

  onClose() {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.poleService.getPoles().subscribe(poleData => this.poles = poleData.data);
  }

}
