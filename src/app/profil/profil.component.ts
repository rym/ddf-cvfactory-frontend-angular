import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/services/users.service';
import { User } from '../shared/models/user';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { EditProfilComponent } from './edit-profil/edit-profil.component';
import { Pole } from '../shared/models/pole';


@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {
  user: User;
  editUserForm: FormGroup;
  nom: string;
  prenom: string;
  constructor(  private dialog: MatDialog, public userService: UserService) {}

  ngOnInit() {
    this.getUser();
  }
  getUser() {
     this.user = this.userService.currentUser;
  }

  editUser(userEmail, userValue) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '550px';
    dialogConfig.data = {
      email: userEmail,
      user: userValue
    };
    const dialogRef = this.dialog.open(EditProfilComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(value => {
      this.getUser();
    });
  }
}
