
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { SideNavComponent } from './shared/layout/side-nav/side-nav.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { AppRoutingModule } from './app-routing.module';

import { SharedModule } from './shared/shared-module';
import { InscriptionComponent } from './authentification/inscription/inscription.component';
import { ConnectionComponent } from './authentification/connection/connection.component';
import { BackendHttpInterceptor } from './shared/interceptor/backend-Http-Interceptor';
import { HomeComponent } from './home/home.component';
import { DialogContentComponent } from './cv/addCv/visualisationCv/dialog-content/dialog-content.component';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { ProfilComponent } from './profil/profil.component';
import { EditProfilComponent } from './profil/edit-profil/edit-profil.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SideNavComponent,
    InscriptionComponent,
    ConnectionComponent,
    DialogContentComponent,
    ProfilComponent,
    EditProfilComponent
  ],
  imports: [
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    BrowserModule,
    LayoutModule,
    SharedModule,
  ],

  entryComponents: [DialogContentComponent, EditProfilComponent],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: BackendHttpInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule {}
