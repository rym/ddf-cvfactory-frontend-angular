export const environment = {
  production: false,
  apiUrlUser : 'https://og6ad99f5i.execute-api.eu-west-1.amazonaws.com/dev/user',
  apiUrlCv : 'https://og6ad99f5i.execute-api.eu-west-1.amazonaws.com/dev/user/{email}',
  apiUrlBarprogress: 'https://og6ad99f5i.execute-api.eu-west-1.amazonaws.com/dev/cvCompleted',
  apiUrlPole: 'https://og6ad99f5i.execute-api.eu-west-1.amazonaws.com/dev/pole'
};
export const COGNITO_KEYS = {
  POOL_ID: 'eu-west-1_1xeAEizNn',
  CLIENT_ID: '646kpiit8idr96umofiue0fg3d',
  REGION: 'eu-west-1'
 };
