// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrlUser : 'https://og6ad99f5i.execute-api.eu-west-1.amazonaws.com/dev/user',
  apiUrlCv : 'https://og6ad99f5i.execute-api.eu-west-1.amazonaws.com/dev/user',
  apiUrlBarprogress: 'https://og6ad99f5i.execute-api.eu-west-1.amazonaws.com/dev/cvCompleted',
  apiUrlPole: 'https://og6ad99f5i.execute-api.eu-west-1.amazonaws.com/dev/pole'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


export const COGNITO_KEYS = {
  POOL_ID: 'eu-west-1_1xeAEizNn',
  CLIENT_ID: '646kpiit8idr96umofiue0fg3d',
  REGION: 'eu-west-1'
 };
